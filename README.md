# Decoupled Drupal Demo Module

### Purpose

To demonstrate how Drupal can be used to serve data that will be rendered and presented outside of the Drupal CMS.

Feel free to use this as a learning tool, or to adapt to your own purposes.

### Background

Drupal as a CMS is an excellent tool for creating and storing content.  Since content is stored in the database, it
can be retrieved and used anywhere.

When a large amount of data needs to be retrieved and shown to the end user, the process from retrieving the data to
displaying it to the end user can become slow.  This is because Drupal's presentation layer is tied into the Drupal
stack.

A faster way to render and present a large amount of data is using "decoupled Drupal".  This term means that content
is presented to the end user outside of Drupal, usually through a JavaScript front end like Node.js, React.js,
Angular.js, etc.

### Endpoint

An endpoint is a url that is accessed from outside of Drupal, where non-HTML data can be retrieved.

By nature, this approach is RESTful, using a $_GET request to retrive data from the Drupal database via an http call.

As with any other $_GET request, the call to the endpoint can contain $_GET variables that can be used as filters and
limiters in a MySQL (or other database) query.

### The Module

A basic Drupal 7 module needs only 2 files, a ".info" file to tell Drupal about the module, and a ".module" file to
contain the hook_menu() call to the endpoint.

The actual code used to retrieve the data from the database can be put in the .module file, but my personal preference
is to use a ".inc" file for this code.  Either approach works.

The **decoupled_drupal_demo.info** file is easy to understand.  As stated above, it just tells Drupal about the module.

For the purposes of this demo module, the **decoupled_drupal_demo.module** file will only be used to initiate
hook_menu().  The sample code and an explanation are directly below.


    $items['decoupled/demo'] = array(
        'title' => 'Decoupled Drupal Demo',
        'page callback' => 'decoupled_drupal_demo_example',
        'access arguments' => array('access content'),
        'delivery callback' => 'drupal_json_output',
        'file' => 'decoupled_drupal_demo_data.inc',
        'type' => MENU_CALLBACK,
    );
 

 * Each new endpoint is defined inside the [] brackets
   * The endpoint in the example code would be http://mysite.com/decoupled/demo.
 * The page callback is the function called when a $_GET request is made to a specific endpoint.
 * The access arguments define what Drupal permission is required to access the endpoint.
 * The delivery callback set to "drupal_json_output" tells Drupal to return JSON instead of HTML.
 * The file is the PHP file containing the source code for the endpoint.
   * Each endpoint will have it's own, unique file.
 * The type defines that this menu item only registers a path to a function.

 The **decoupled_drupal_demo_data.inc** file contains all code associated with our page callback function in our menu
 item.

 The purpose of the PHP code on this page is to return a status message and data to the endpoint.

 **NOTE**:  Normally, a developer would make a database query to the Drupal database to get data to return to the
 endpoint.  For this demo module, I'm manufacturing the data.

 I prefer the modular approach to development, so I've created a separate function (aside from my page callback
 function) that will actually return data.  Ostensibly, I could call this function from anywhere in Drupal and use it
 to return data.  So line 18 of **decoupled_drupal_demo_data.inc** makes a call to another function.

 It's important to remember that when making a query against a database, returning 0 rows is a successful query, the
 same as returning 100 rows would be. The only problem we have is if we return nothing, or NULL.

 It's a good practice never to assume that data is returned, so as a developer you should have a method you feel
 comfortable with for checking the result of your query.

 On line 20 of **decoupled_drupal_demo_data.inc** I check to see if $result is set (an object with 0+ rows).  If it is,
 I set the state to 1, or TRUE.  Otherwise, I set it to 0, or FALSE, and include a message about what went wrong.  The
 "info" key is included when state = 1 for consistency.'

### Assembling the Payload

At the very end of the page callback function, I assemble the payload to return to the endpoint.

I've included $payload['message'] for the users of this demo module.  It's just a little extra info.

The $payload['status'] can be checked on the front end; if it's 0 or FALSE, something bad happened, and there is no
need to go further.

If the $payload['status'] is 1 or TRUE, a good next step would be to count the number of objects returned.  Remember,
returning 0 rows from a query is a possibility, and is still a success.

Since it is possible for something to go wrong, and literally nothing get returned, I don't like to assume there is data
to return in $result, so on line 38 of  **decoupled_drupal_demo_data.inc**, I check $status['state'], and only if it's
1 or TRUE do I set $payload['data'] to the value of $result.

### Rendering the Returned Data

Use whatever front end tool you prefer to render the returned data at the url of your location.