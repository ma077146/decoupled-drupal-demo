<?php

/**
 * @file
 * Decoupled Demo Data.
 */

/**
 * This is the function called when the endpoint is accessed.
 *
 * @return array
 *    Returns data to the endpoint.
 */
function decoupled_drupal_demo_example() {
  // Create an array to hold data to return.
  $payload = array();

  // Disassemble the $_GET variables.
  if (isset($_GET['data']) && is_numeric($_GET['data'])) {
    // 1 = manufactured data; 2 = database data.
    $data = $_GET['data'];
  }

  // For database queries only.
  if (isset($_GET['offset']) && is_numeric($_GET['offset'])) {
    $offset = $_GET['offset'];
  }

  // For database query or manufactured data.
  if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
    $limit = $_GET['limit'];
  }

  // We want to time how long it takes to retrieve data.
  $start = microtime();

  // Send value of 2 to get database data.
  if ($data == 2) {
    $result = _decoupled_drupal_demo_employees_data_return($offset, $limit);
  }
  else {
    $result = _decoupled_drupal_demo_data_return($limit);
  }

  // Stop our timer.
  $stop = microtime();

  // How many rows of data?
  $records = count($result);

  if (isset($result)) {
    $status = array(
      'state' => 1,
      'info' => '',
    );
  }
  else {
    $status = array(
      'state' => 0,
      'info' => t('Could not retrieve decoupled data.'),
    );
  }

  $payload['message'] = 'This demo displays only JSON data; use a JavaScript ' .
    'library to render the returned data to the end user at another url.';

  // Assemble the payload.
  $payload['status'] = $status;
  $payload['elapsed time'] = $stop - $start;
  $payload['count'] = $records;

  if ($status['state'] == 1) {
    $payload['data'] = $result;
  }

  return $payload;
}

/**
 * Return some data; normally from database, but generated for demo.
 *
 * @return array
 *    Randomly generated data.
 */
function _decoupled_drupal_demo_data_return($limit) {
  // Normally you would query the database here to return data.  Since this is
  // a demo, we're just generating some data to return.
  $data = array();

  $l = (isset($limit)) ? $limit : 10;

  $y = 1;

  for ($x = 2; $x <= $l; $x++) {
    $data['row ' . $y]['base_num'] = number_format($x);
    $data['row ' . $y]['x2'] = number_format($x * 2);
    $data['row ' . $y]['x4'] = number_format($x * 4);
    $data['row ' . $y]['x10'] = number_format($x * 10);
    $data['row ' . $y]['squared'] = number_format($x * $x);
    $data['row ' . $y]['cubed'] = number_format($x * $x * $x);

    $y++;
  }

  return $data;
}


/**
 * Return some data from the database.
 * @param  integer $offset
 *    Offset for query.
 * @param  integer $limit
 *    Limit for query.
 *
 * @return array
 *    Data from employees table.
 */
function _decoupled_drupal_demo_employees_data_return($offset, $limit) {
  $o = (isset($offset)) ? $offset : 0;
  $l = (isset($limit)) ? $limit : 50;

  $query = db_select('employees', 'e');
  $query->fields('e');
  $query->range($o, $l);

  $result = $query->execute()->fetchAll();

  return $result;
}
